FROM archlinux:base-devel

# update the base system and install tools for building packages
RUN pacman -Syu --needed --noconfirm git \
    && find /var/cache/pacman/ -type f -delete

# add and switch to a non-root user in order to build packages
RUN useradd -G wheel -ms /bin/bash docker \
 && echo "docker ALL=NOPASSWD:/usr/bin/pacman,/usr/bin/paru" >> /etc/sudoers
WORKDIR /home/docker
USER docker

# install pgtk-gcc emacs
# Install the AUR helper paru: https://github.com/Morganamilo/paru
# If you do not wish to use the AUR modify as necesarry.
RUN git clone https://aur.archlinux.org/paru-bin.git
RUN chmod a+w ./paru-bin
# Paru can only be built as a regular user with sudo access
# This will build paru and install
RUN cd paru-bin && \
	makepkg -sicr --noconfirm

# Cleanup
RUN paru -Scc --noconfirm
RUN rm -rf ./paru-bin

# install quality of life utilities + gtk3 for broadwayd
RUN sudo paru -Syu --noconfirm ripgrep fd bat gtk3 starship

# attempt to improve compilation time
RUN echo "Using `grep -c ^processor /proc/cpuinfo` parallel jobs" 
#RUN $XDG_CONFIG_HOME/pacman/makepkg.conf

# install emacs-pgtk-native-comp
RUN MAKEFLAGS="-j `grep -c ^processor /proc/cpuinfo`" \
    paru -Sy --noconfirm emacs-pgtk-native-comp-git \
    > ./log_emacs-pgtk-native-comp-git.txt && \
    rm -rf emacs-pgtk-native-comp-git

# expose a volume for sharing data
VOLUME /local/data

# GDK/broadway settings
ENV GDK_BACKEND broadway
ENV BROADWAY_DISPLAY :5
EXPOSE 51313
CMD nohup broadwayd -p 51313 -a 0.0.0.0 :5 & \
    emacs
